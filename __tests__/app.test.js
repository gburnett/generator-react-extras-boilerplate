"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

function setup(options) {
  return helpers
    .run(path.join(__dirname, "../generators/app"))
    .withPrompts(options);
}

describe("generator-javascript-project:app", () => {
  describe("in a yarn project", () => {
    beforeEach(() =>
      setup({ packageManager: "yarn", saga: false, reach: false })
    );

    it("creates files", () => {
      assert.file([
        "src/providers/createState.js",
        "src/providers/createStoreProvider.js",
        "src/providers/createAppProvider.js",
        "src/model/template/slice.js",
        "plopfile.js"
      ]);
      assert.noFile([
        "src/providers/createLocationProvider.js",
        "src/sagas/main.js"
      ]);
    });
  });

  describe("in an npm project", () => {
    beforeEach(() =>
      setup({ packageManager: "npm", saga: false, reach: false })
    );

    it("creates files", () => {
      assert.file([
        "src/providers/createState.js",
        "src/providers/createStoreProvider.js",
        "src/providers/createAppProvider.js",
        "src/model/template/slice.js",
        "plopfile.js"
      ]);
      assert.noFile([
        "src/providers/createLocationProvider.js",
        "src/sagas/main.js"
      ]);
    });
  });

  describe("with sagas", () => {
    beforeEach(() =>
      setup({ packageManager: "yarn", saga: true, reach: false })
    );

    it("creates redux-saga files", () => {
      assert.file(["src/providers/createState.js", "src/sagas/main.js"]);
      assert.noFile("src/providers/createLocationProvider.js");
      assert.fileContent(
        "src/providers/createState.js",
        /createSagaMiddleware/
      );
    });
  });

  describe("with reach router", () => {
    beforeEach(() =>
      setup({ packageManager: "yarn", saga: false, reach: true })
    );

    it("creates reach router files", () => {
      assert.file([
        "src/providers/createLocationProvider.js",
        "src/providers/createState.js"
      ]);
      assert.noFile("src/sagas/main.js");
      assert.fileContent("src/providers/createState.js", /history/);
    });
  });

  describe("with both sagas and reach router", () => {
    beforeEach(() =>
      setup({ packageManager: "yarn", saga: true, reach: true })
    );

    it("creates reach router files", () => {
      assert.file([
        "src/providers/createLocationProvider.js",
        "src/providers/createState.js",
        "src/sagas/main.js"
      ]);
      assert.fileContent("src/providers/createState.js", /history/);
      assert.fileContent(
        "src/providers/createState.js",
        /createSagaMiddleware/
      );
    });
  });
});
