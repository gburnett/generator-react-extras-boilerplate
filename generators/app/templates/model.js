import { createSlice } from "@reduxjs/toolkit";

const initialState = { value: "initial", context: { meaningful: null } };
const name = "template";

const template = createSlice({
  initialState,
  name,
  reducers: {
    meaningless: (state, action) => state
  }
});

const { reducer, actions } = template;

export { reducer, actions, name };
