import React from "react";
import { Provider } from "react-redux";

const createStoreProvider = store => ({ children }) => (
  <Provider store={store}>{children}</Provider>
);

export { createStoreProvider };
