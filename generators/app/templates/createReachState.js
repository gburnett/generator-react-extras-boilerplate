import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { reducer as template } from "../model/template/slice";
import { createReduxHistoryContext, reachify } from "redux-first-history";

const createState = ({ preloadedState, history }) => {
  const {
    createReduxHistory,
    routerMiddleware,
    routerReducer: router
  } = createReduxHistoryContext({
    history
  });

  const store = configureStore({
    preloadedState,
    reducer: { template, router },
    middleware: [...getDefaultMiddleware(), routerMiddleware]
  });

  return {
    store,
    history: reachify(createReduxHistory(store))
  };
};

export { createState };
