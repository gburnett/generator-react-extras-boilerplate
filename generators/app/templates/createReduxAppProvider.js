import React from "react";
import { createStoreProvider } from "./createStoreProvider";
import { createState } from "./createState";

const createAppProvider = (state = {}) => ({ children }) => {
  const { store } = createState(state);
  const StoreProvider = createStoreProvider(store);

  return <StoreProvider>{children}</StoreProvider>;
};

export { createAppProvider };
