import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { main } from "../sagas/main";
import { reducer as template } from "../model/template/slice";

const createState = ({ preloadedState = {} }) => {
  const sagaMiddleware = createSagaMiddleware();
  const store = configureStore({
    preloadedState,
    reducer: { template },
    middleware: [...getDefaultMiddleware(), sagaMiddleware]
  });

  sagaMiddleware.run(main);

  return { store };
};

export { createState };
