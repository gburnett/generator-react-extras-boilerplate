module.exports = function(plop) {
  plop.load('@gburnett/plop-react-component');
  plop.load('@gburnett/plop-react-redux-component');
};
