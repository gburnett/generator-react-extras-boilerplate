import React from "react";
import { LocationProvider } from "@reach/router";

const createLocationProvider = history => ({ children }) => (
  <LocationProvider history={history}>{children}</LocationProvider>
);

export { createLocationProvider };
