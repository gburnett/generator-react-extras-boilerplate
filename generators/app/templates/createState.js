import { configureStore } from "@reduxjs/toolkit";
import { reducer as template } from "../model/template/slice";

const createState = ({ preloadedState = {} }) => {
  const store = configureStore({
    preloadedState,
    reducer: { template }
  });

  return { store };
};

export { createState };
