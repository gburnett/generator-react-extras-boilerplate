import React from "react";
import { createLocationProvider } from "./createLocationProvider";
import { createStoreProvider } from "./createStoreProvider";
import { createState } from "./createState";

const createAppProvider = (state = {}) => ({ children }) => {
  const { store, history } = createState(state);
  const LocationProvider = createLocationProvider(history);
  const StoreProvider = createStoreProvider(store);

  return (
    <StoreProvider>
      <LocationProvider>{children}</LocationProvider>
    </StoreProvider>
  );
};

export { createAppProvider };
