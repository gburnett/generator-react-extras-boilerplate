import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { reducer as template } from "../model/template/slice";
import { main } from "../sagas/main";
import { createReduxHistoryContext, reachify } from "redux-first-history";

const createState = ({ preloadedState, history }) => {
  const {
    createReduxHistory,
    routerMiddleware,
    routerReducer: router
  } = createReduxHistoryContext({
    history
  });

  const sagaMiddleware = createSagaMiddleware();

  const store = configureStore({
    preloadedState,
    reducer: { template, router },
    middleware: [...getDefaultMiddleware(), routerMiddleware, sagaMiddleware]
  });

  sagaMiddleware.run(main);

  return {
    store,
    history: reachify(createReduxHistory(store))
  };
};

export { createState };
