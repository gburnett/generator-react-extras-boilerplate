import { call } from "redux-saga/effects";

function* main() {
  yield call(window.alert, "Objectionable saga running!");
}

export { main };
