const Generator = require("yeoman-generator");

const YARN = "yarn";
const NPM = "npm";

module.exports = class extends Generator {
  async prompting() {
    this.answers = await this.prompt([
      {
        type: "list",
        name: "packageManager",
        message: "Which package manager?",
        choices: [YARN, NPM],
        store: true
      },
      {
        type: "confirm",
        name: "saga",
        message: "Using redux-saga ?"
      },
      {
        type: "confirm",
        name: "reach",
        message: "using @reach/router ?"
      }
    ]);
  }

  install() {
    const saga = this.answers.saga ? ["redux-saga"] : [];
    const reach = this.answers.reach
      ? ["history", "@reach/router", "redux-first-history"]
      : [];
    const dependencies = ["react-redux", "@reduxjs/toolkit", ...saga, ...reach];
    const devDependencies = [
      "plop",
      "@gburnett/plop-react-component",
      "@gburnett/plop-react-redux-component"
    ];

    if (this.answers.packageManager === YARN) {
      this.yarnInstall(dependencies);
      this.yarnInstall(devDependencies, { dev: true });
    } else if (this.answers.packageManager === NPM) {
      this.npmInstall(dependencies);
      this.npmInstall(devDependencies, { saveDev: true });
    } else {
      throw new Error("Whoops! No package manager!");
    }
  }

  writing() {
    const appProvider = this.answers.reach
      ? "createReduxReachAppProvider.js"
      : "createReduxAppProvider.js";
    let createState = "createState.js";

    if (this.answers.reach && this.answers.saga) {
      createState = "createReachSagaState.js";
    } else if (this.answers.reach) {
      createState = "createReachState.js";
    } else if (this.answers.saga) {
      createState = "createSagaState.js";
    }

    this.fs.copyTpl(
      this.templatePath("plopfile.js"),
      this.destinationPath("plopfile.js")
    );

    this.fs.copyTpl(
      this.templatePath(createState),
      this.destinationPath("src/providers/createState.js")
    );

    this.fs.copyTpl(
      this.templatePath("createStoreProvider.js"),
      this.destinationPath("src/providers/createStoreProvider.js")
    );

    if (this.answers.reach) {
      this.fs.copyTpl(
        this.templatePath("createLocationProvider.js"),
        this.destinationPath("src/providers/createLocationProvider.js")
      );
    }

    if (this.answers.saga) {
      this.fs.copyTpl(
        this.templatePath("saga.js"),
        this.destinationPath("src/sagas/main.js")
      );
    }

    this.fs.copyTpl(
      this.templatePath(appProvider),
      this.destinationPath("src/providers/createAppProvider.js")
    );

    this.fs.copyTpl(
      this.templatePath("model.js"),
      this.destinationPath("src/model/template/slice.js")
    );
  }
};
